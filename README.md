# curso-docker #

<!-- TOC -->

- [curso-docker](#curso-docker)
- [Requisitos](#requisitos)
- [Instruções de Uso](#instruções-de-uso)
- [Desenvolvedor](#desenvolvedor)
- [Licença](#licença)

<!-- TOC -->

Este é o repositório de execícios referentes à parte prática do seminário sobre Docker apresentado na disciplina de **Sistemas Distribuídos** durante o [Mestrado Profissional em Tecnologia da Informação do IFPB](https://estudante.ifpb.edu.br/cursos/195/). A disciplina foi ministrada pela professora Dra. Luciana Oliveira em 12/06/2019.


# Requisitos

1. Crie uma conta gratuita no Google Cloud (http://cloud.google.com).

# Instruções de Uso

* Baixe os códigos com o seguinte comandoou acesse a página https://gitlab.com/cursos_/seminario_docker/tags e o obtenha o pacote referente a tag mais recente.

```bash
git clone https://gitlab.com/cursos_/seminario_docker.git
cd seminario_docker
```

* Execute os exercícios práticos na ordem citada ao longo do curso.

Mais informações sobre o Docker podem ser obtidos nos seguintes links:

* http://blog.aeciopires.com/tag/docker/
* http://blog.aeciopires.com/primeiros-passos-com-docker/
# Desenvolvedor

Aécio dos Santos Pires

Site: http://aeciopires.com

# Licença

Copyright (c) 2021 Aécio dos Santos Pires
