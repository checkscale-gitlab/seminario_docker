# Objetivos

O objetivo deste exercício é mostrar como gerenciar volumes no Docker Host e mapeá-los com contêineres para evitar a perda de dados quando os contêineres forem removidos.

Será apresentada apenas uma forma de gerenciar volumes no Docker, mas existem outras. 

No caso, o volume será criado manualmente a partir de um diretório no Docker Host e mapeando para um diretório específico dentro do contêiner, no qual uma aplicação deve ser desenvolvida e configurada para salvar os dados. Assim quando a aplicação escrever os dados no diretório específico do contêiner, automaticamente esses mesmos dados estarão sendo escritos no Docker Host. Isso facilita o backup dos dados.

Você também verá que é possível usar uma mesma imagem Docker para criar contêineres diferentes para disponibilizar o mesmo serviço para clientes diferentes.

# Usando volumes a partir de um diretório do Docker Host.

Crie os diretórios ``/opt/docker/site1`` e ``/opt/docker/site2``.

```bash
sudo mkdir -p /opt/docker/site1
sudo mkdir -p /opt/docker/site2
```

Inicie o contêiner do Tomcat.

```bash
docker run -d --rm -p 8888:8080 --name=tomcat tomcat:8.5-jre8
```

Acesse o Tomcat no navegador pela URL http://IP-SERVIDOR:8888.

Pare o Tomcat.

```bash
docker stop tomcat
```

Verifique se o contêiner foi removido. Se sim, qual o motivo disso?

```bash
docker ps
docker ps -a
```

Copie o diretório ``ROOT``, existente no repositório para os diretórios criados no início do exercício.

```bash
git clone https://gitlab.com/cursos_/seminario_docker.git
cd seminario_docker
sudo cp -R pratica3/ROOT /opt/docker/site1/
sudo cp -R pratica3/ROOT /opt/docker/site2/
```

Inicie o contêiner do Tomcat mapeando um volume a ser criado no diretorio anterior do Docker Host e montando em uma pasta específica do contêiner.

```bash
docker run -d --rm -p 8888:8080 --name=tomcat -v /opt/docker/site1/ROOT:/usr/local/tomcat/webapps/ROOT tomcat:8.5-jre8
```

Configure uma regra de firewall no Google Cloud para permitir o acesso remoto a porta 8888/TCP da instância criada. Mais informações serão passadas pelo instrutor.

Acesse novamente o Tomcat no navegador pela URL http://IP-SERVIDOR:8888 e verifique se algo mudou.
Se sim, você acaba de fazer um deploy da aplicação exemplo no Tomcat, sobrescrevendo a aplicação padrão pela existente em um volume do Docker Host. Essa aplicação será persistida quando o contêiner for removido e poderá ser usado em outro contêiner.

Inicie outro contêiner do Tomcat para disponibilizar o site 2.

```bash
docker run -d --rm -p 8889:8080 --name=tomcat2 -v /opt/docker/site2/ROOT:/usr/local/tomcat/webapps/ROOT tomcat:8.5-jre8
```

Configure uma regra de firewall no Google Cloud para permitir o acesso remoto a porta 8889/TCP da instância criada. Mais informações serão passadas pelo instrutor.

Abra outra aba do navegador e acesse a URL: http://IP-SERVIDOR:8889/

É a mesma aplicação?

Pare os contêiners do Tomcat.

```bash
docker stop tomcat
docker stop tomcat2
```

Remova os contêiners do Tomcat.

```bash
docker rm tomcat
docker rm tomcat2
```

Verifique se a aplicação exemplo ainda está nos diretórios ``/opt/docker/site1`` e ``/opt/docker/site2``.